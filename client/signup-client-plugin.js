import loadjs from "loadjs"

function register({ registerHook, peertubeHelpers }) {
  initMTC(registerHook, peertubeHelpers).catch(err =>
    console.error("Cannot initialize mtcaptcha plugin", err)
  )
}

export { register }

function initMTC(registerHook, peertubeHelpers) {
  return peertubeHelpers.getSettings().then(async s => {
    if (!s || !s["mtcaptcha-site-key"]) {
      console.error(
        "MTCaptcha plugin lacks a site key set to use the MTCaptcha. Falling back to normal registration."
      )
      return
    }

    // add captcha to the first form (user form)
    const node = document.getElementsByTagName("form")[0]
    const div = document.createElement("div")
    div.setAttribute("class", "mtcaptcha")
    node.appendChild(div)

    window.mtcaptchaConfig = {
      "sitekey": s["mtcaptcha-site-key"],
      "render": "auto",
      "customStyle": {
            "cardColor": "#f5f5f5",
            "cardBorder": "#9EA1A0",
            "placeHolderColor": "#F89300",
            "inputTextColor": "#F87800",
            "inputBackgroundColor": "#fafafa",
            "inputBorderColor": {
                  "byDefault": "#F89300",
                  "hover": "#f07000",
                  "active": "#f07000"
                 },
            "buttonIconColor": {
                  "refresh": "#dddddd",
                  "verify": "#55caf1",
                  "audio": "#F89300",
                  "audiofocus": "#f07000"
                 }
           },
      "verified-callback": state => window.MTCaptchaLoadCallbackResponse = state.verifiedToken,
      "renderQueue": []
    }

    loadjs([
      "https://service.mtcaptcha.com/mtcv1/client/mtcaptcha.min.js",
      "https://service2.mtcaptcha.com/mtcv1/client/mtcaptcha2.min.js",
    ])

    registerHook({
      target: "filter:api.signup.registration.create.params",
      handler: body =>
        Object.assign({}, body, {
          "mtcaptcha-response": window.MTCaptchaLoadCallbackResponse
        })
    })
  })
}
