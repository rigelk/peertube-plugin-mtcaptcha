const get = require("simple-get")

async function register({
  registerHook,
  registerSetting,
  settingsManager
}) {
  // see https://www.mtcaptcha.com/dev-guide-quickstart
  registerSetting({
    name: 'mtcaptcha-site-key',
    label: 'MTCaptcha Site Key',
    type: 'input',
    private: false
  })
  registerSetting({
    name: 'mtcaptcha-secret-key',
    label: 'MTCaptcha Secret Key',
    type: 'input',
    private: true
  })

  registerHook({
    target: "filter:api.user.signup.allowed.result",
    handler: (result, params) => verifyCaptcha(result, params, settingsManager)
  })
}

async function unregister() {
  return
}

module.exports = {
  register,
  unregister
}

async function verifyCaptcha (result, params, settingsManager) {
  // mtcaptcha-response is the key that browser will generate upon form submit.
  // if its blank or null means user has not selected the captcha, so return the error.
  if (!params.body["mtcaptcha-response"]) {
    return { allowed: false, errorMessage: "Captcha wasn't filled" }
  }

  const secretKey = await settingsManager.getSetting('mtcaptcha-secret-key')
  if (!secretKey) return result

  // params.connection.remoteAddress will provide IP address of connected user.
  const verificationUrl =
    "https://service.mtcaptcha.com/mtcv1/api/checktoken?privateKey=" +
    secretKey +
    "&token=" +
    params.body["g-recaptcha-response"]

  return get(verificationUrl, function (err, res, body) {
    body = JSON.parse(body)
    if (body.success !== undefined && !body.success) {
      return { allowed: false, errorMessage: "Wrong captcha" }
    }
    return result
  })
}
